#include <RCSwitch.h>

#define EMETTEUR 12 //le port de l'émetteur importe peu, ici c'est le port 12 sur une arduino Uno mais vous pouvez mettre n'importe quel port Digital (PWM)

RCSwitch mySwitch = RCSwitch();

void setup() {

  Serial.begin(9600);
  mySwitch.enableReceive(0);  // on active la réception sur le pin 2 (qui correspond à l'interruption 0)

}

//écoute d'un signal et transmission sur le port série
void snif() {
  while (1) {
    if (Serial.available())
      serialEvent();
    if (mySwitch.available()) {
      Serial.print("Recu : ");
      Serial.print( mySwitch.getReceivedValue() );
      Serial.print(" / ");
      Serial.print( mySwitch.getReceivedBitlength() );
      Serial.print(" bits ");
      Serial.print("Protocole : ");
      Serial.print( mySwitch.getReceivedProtocol() );
      Serial.print(" temps : ");
      Serial.print( mySwitch.getReceivedDelay() );
      Serial.println(" microsecondes");
      mySwitch.resetAvailable();
    }
  }
}


void replay() {
    mySwitch.enableTransmit(EMETTEUR);
    mySwitch.setRepeatTransmit(5);
    int p,l,t;
    long s;
    while (1) {
      
      while(!Serial.available());
      String sig = Serial.readString();
      if (sig == "stop")
        exit(0);
      else
        s = sig.toInt();
        
      while(!Serial.available());
      String taille = Serial.readString();
      t = taille.toInt();
      
      while(!Serial.available());
      String protocole = Serial.readString();
      p = protocole.toInt();
      
      while(!Serial.available());
      String longueur = Serial.readString();
      l = longueur.toInt();

      
      mySwitch.setProtocol(p);
      mySwitch.setPulseLength(l);

      mySwitch.send(s, t);
      delay(10);
  }
}

//génération de signaux carrés duty cycle pour du brouillage 433MHz
void jamming() { 
  while (1) {
    if (Serial.available())
      serialEvent();
    tone(EMETTEUR, 6700);
    tone(EMETTEUR, 10000);
    tone(EMETTEUR, 1000);
  }
  
}

void attente() {
  while (1) {
    if (Serial.available())
      serialEvent();
  }
}

void serialEvent() {
  noTone(EMETTEUR);
  int m = Serial.read();
  switch (m) {
    case 115: snif(); //115 : lettre s
              break;
    case 114: replay(); //114 : lettre r
              break;
    case 106: jamming(); //106 : lettre j
              break;
    case 113: attente(); //113 : lettre q
              break;
    default: snif();
  }
  
  
}


void loop() {


}
