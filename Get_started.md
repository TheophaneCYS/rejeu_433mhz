# Instructions pour utiliser le projet

(plus de détails dans la documentation)

- **Prérequis**  
Matériel : Une carte arduino (peut importe le modèle), un émetteur et un récepteur 433MHz, des câbles, une pile 9V, et un système à tester 433MHz.  
Logiciel : Librairie arduino RC-Switch (à installer depuis l'IDE arduino).

- **Branchements**  
Récepteur : alimentation > 5V et GND de l'arduino. DATA > pin 2.  
Émetteur : alimentation > pile 9V. DATA > pin 12.  
Relier l'arduino au PC.

- **Récupérer le dépôt**  
``` SH
git clone git@gitlab.com:TheophaneCYS/rejeu_433mhz.git
cd rejeu_433mhz
```

- **Build le docker**
``` SH
sudo docker build -t <nom> -f Dockerfile .
```

- **Run le docker**
``` SH
sudo docker run -it --device=/dev/ttyACM0 -e LANG=C.UTF-8 <nom>
```
Où /dev/ttyACM0 est le port sur lequel est relié l'arduino.

- **Lancer l'application**  
Une fois dans le conteneur :  
``` SH
cd script
python3 attaques_lpd.py <argument>
```
Où argument est le mode dans lequel vous souhaitez que l'arduino soit :
- scan
- replay
- jamming
