\babel@toc {french}{}
\contentsline {chapter}{\numberline {1}Cahier des charges}{2}{chapter.1}%
\contentsline {section}{\numberline {1.1}Présentation du sujet}{2}{section.1.1}%
\contentsline {section}{\numberline {1.2}Organisation du projet}{2}{section.1.2}%
\contentsline {section}{\numberline {1.3}Les tâches réalisées}{2}{section.1.3}%
\contentsline {chapter}{\numberline {2}Fonctionnement et attaques de portières de voiture}{4}{chapter.2}%
\contentsline {section}{\numberline {2.1}La carte HackRF}{4}{section.2.1}%
\contentsline {section}{\numberline {2.2}Fonctionnement d'une ouverture à distance}{4}{section.2.2}%
\contentsline {subsection}{\numberline {2.2.1}Premier cas}{4}{subsection.2.2.1}%
\contentsline {subsection}{\numberline {2.2.2}Second cas}{5}{subsection.2.2.2}%
\contentsline {section}{\numberline {2.3}Principe de l'attaque}{5}{section.2.3}%
\contentsline {subsection}{\numberline {2.3.1}Attaque du premier cas}{5}{subsection.2.3.1}%
\contentsline {subsection}{\numberline {2.3.2}Attaque du second cas}{5}{subsection.2.3.2}%
\contentsline {chapter}{\numberline {3}Étude de faisabilité}{6}{chapter.3}%
\contentsline {section}{\numberline {3.1}Les véhicules vulnérables}{6}{section.3.1}%
\contentsline {subsection}{\numberline {3.1.1}Clé mains-libres}{6}{subsection.3.1.1}%
\contentsline {subsection}{\numberline {3.1.2}Plip}{6}{subsection.3.1.2}%
\contentsline {section}{\numberline {3.2}Les véhicules moins vulnérables}{7}{section.3.2}%
\contentsline {chapter}{\numberline {4}PoC sur la vulnérabilité des LPD433}{8}{chapter.4}%
\contentsline {section}{\numberline {4.1}Que sont les LPD433 ?}{8}{section.4.1}%
\contentsline {section}{\numberline {4.2}Les vulnérabilités et ce quelles impliquent}{8}{section.4.2}%
\contentsline {section}{\numberline {4.3}Exploiter ces vulnérabilités}{9}{section.4.3}%
\contentsline {subsection}{\numberline {4.3.1}La faisabilité}{9}{subsection.4.3.1}%
\contentsline {subsection}{\numberline {4.3.2}Les différentes attaques}{9}{subsection.4.3.2}%
\contentsline {subsubsection}{\numberline {4.3.2.1}Capture}{9}{subsubsection.4.3.2.1}%
\contentsline {subsubsection}{\numberline {4.3.2.2}Rejeu}{10}{subsubsection.4.3.2.2}%
\contentsline {subsubsection}{\numberline {4.3.2.3}Brouillage}{10}{subsubsection.4.3.2.3}%
\contentsline {section}{\numberline {4.4}Conclusion}{10}{section.4.4}%
\contentsline {chapter}{\numberline {5}Réalisation du PoC}{11}{chapter.5}%
\contentsline {section}{\numberline {5.1}Code Arduino et Python}{11}{section.5.1}%
\contentsline {section}{\numberline {5.2}Tests de l'application}{11}{section.5.2}%
\contentsline {section}{\numberline {5.3}Docker}{11}{section.5.3}%
\contentsline {chapter}{\numberline {6}Conclusion}{12}{chapter.6}%
