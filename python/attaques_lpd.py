#!/usr/bin/python3.8

# auteur : Théophane LHOMMELET

#   Script comprennant 3 modes d'attaques pour les systèmes LPD fonctionnant à une fréquence de
# 433MHz.
#   1er mode : scan -> on écoute ce qu'il se passe et on affiche les signaux de télécommandes
# reçu par la carte arduino.
#   2ème mode : replay -> on saisit un signal à envoyer avec l'émetteur. Cela permet de renvoyer
# un des signaux reçus avec le mode scan.
#   3ème mode : jamming -> on effectue du brouillage afin de rendre hors service les systèmes 
# qui fonctionnent avec la fréquence de 433MHz.

#avant d'exécuter le script, vous pouvez lire la documentation sur le gitlab (https://gitlab.com/TheophaneCYS/rejeu_433mhz)
#où vous y trouverez également les branchements pour la carte arduino (et le code dans le dossier arduino).

import sys #pour récupérer les arguments
import serial #pour communiquer avec le port série
import signal #pour capturer le controle C
import time #pour ajouter un délai

def usage():
    """ fonction pour indiquer comment faire fonctionner le script et pour le fermer
        (à appeler lors d'une erreur sur l'utilisation du script)"""
    print("Usage : "+sys.argv[0]+" mode")
    print("modes possibles : scan, replay, jamming")
    exit()


def signal_handler(sig, frame):
    """capture d'un signal, utilisé ici pour capturer un SIGINT (controle C), car on souhaite fermer le fichier proprement lors d'un controle C. On envoie également
    un signal à l'arduino pour lui dire d'arrêter ce qu'elle fait"""
    print('Fin du programme.')
    fichier.close()
    ser.write("q".encode("ascii")) #la lettre q indique à l'arduino de ne rien faire et d'attendre un prochain appel
    time.sleep(0.1) #pour attendre que le message s'est bien envoyé et pour laisser le temps à l'arduino de changer de mode (ne fonctionne pas bien sans attente)
    sys.exit(0)

def verification():
    if (len(sys.argv) != 2): #il faut 1 argument 
        usage()

    if (sys.argv[1] not in ["scan", "replay", "jamming"]): #l'argument doit être un des 3 mots cités
        usage()


def main():
    """lancement d'un des trois modes de fonctionnement de l'arduino en fonction de l'argument"""
    if (sys.argv[1] == "scan"):


        print("\nMode scan : vérifiez que la carte arduino est bien branchée à l'ordinateur et que le récepteur est branché sur la broche 2. Utilisez une antenne pour une meilleure réception. \n Les signaux reçus seront affichés et  stockés dans un fichier nommé signaux.txt \n Pour arrêter le scan : controle C.")

        message = "s"
        ser.write(message.encode('ascii')) #la lettre s indique à l'arduino de se mettre en mode scan
        time.sleep(0.1)


        while (1):
            ligne = ser.readline()
            ligne = ligne.decode()
            if (ligne != ""):
                print(ligne)
                fichier.write(ligne)



    elif (sys.argv[1] == "replay"):

        print("\nMode replay : vérifiez que la carte arduino est bien branchée à l'ordinateur et que l'émetteur est branché à la broche 12. Il est conseillé d'alimenter l'émetteur au delà du 5V de l'USB pour une meilleure émission du signal. L'alimentation avec une pile 9V est très bien, mais vérifiez sur la datasheet du composant la tension maximale. Vous pouvez utiliser une antenne pour une meilleure émission.\nRentrez \"stop\" pour arrêter.")
        
        message = "r"
        ser.write(message.encode("ascii")) #la lettre r indique à l'arduino de se mettre 
        time.sleep(0.1)

        signal = "blabla"
        while (signal != "stop"):
            signal = input("Saisir le signal à envoyer en format décimal (saisir stop pour arrêter) : ")
            ser.write(signal.encode('ascii')) #on envoie d'abord le signal, si c'est "stop", la arduino va s'arrêter
            if (signal != "stop"):
                taille = input("Saisir la taille du signal (nombre de bits) : ")
                ser.write(taille.encode('ascii'))
                protocole = input("Saisir le protocole (tapez 1 si inconnu) : ")
                ser.write(protocole.encode('ascii'))
                longueur = input("Saisir la longueur en microsecondes du signal (tapez 300 si inconnu) : ")
                ser.write(longueur.encode('ascii'))
            

        

    elif (sys.argv[1] == "jamming"):

        print("\nMode jamming : vérifiez que la carte arduino est bien branchée à l'ordinateur et que l'émetteur est branché à la broche 12. Il est conseillé d'alimenter l'émetteur au delà du 5V de l'USB pour une meilleure émission du bruit. L'alimentation avec une pile 9V est très bien, mais vérifiez sur la datasheet du composant la tension maximale. Vous pouvez utiliser une antenne pour une meilleure émission.")

        message = "j"
        ser.write(message.encode("ascii")) #la lettre j indique à l'arduino de se mettre en mode jamming
        time.sleep(0.1) #pour attendre que le message s'est bien envoyé et pour laisser le temps à l'arduino de changer de mode (ne fonctionne pas bien sans attente)


        #une fois que la carte est en mode jamming, celle-ci émet en continue.
        #On va donc faire une boucle infinie pour des raisons de sécurités, et lorsque la
        #boucle s'arrêtera (avec un controle C) un signal sera envoyé à l'arduino afin
        #de stopper le brouillage. Car on pourrait l'oublier et déranger des gens avec.


        while (1):
            pass



if __name__ == "__main__":

    verification()
    #création du fichier et liaison port série
    signal.signal(signal.SIGINT, signal_handler)
    port = input("Saisir le port (exemple : /dev/ttyACM0 sur linux et COM3 sur windows) : ")
    ser = serial.Serial(port, baudrate=9600, timeout=1)
    time.sleep(2.0) #cela prend un peu de temps de se connecter à l'arduino
    fichier = open("signaux.txt", "a")
    main()


