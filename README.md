# Rejeu de signaux sur des systèmes domotiques


Étude des LPD (low power device), qui sont de petits systèmes peu coûteux et communiquant à distance sur une fréquence généralement de 433MHz.  

Utilisation d'une carte arduino, d'un émetteur/récepteur 433MHz et d'un ordinateur pour réaliser les différents scénarios :  
- capture d'un signal (snif)
- rejeu d'un signal
- brouillage


## Ce que contient le projet :

Dossier **arduino** > contient le code arduino à téléverser dans la carte  
Dossier **application** > contient le code python de l'application dans un conteneur docker  
Dossier **Documentation** > contient les explications techniques pour refaire tout le projet (doc sphinx)  
Dossier **Rapport** > contient le rapport LaTeX du projet qui explique les attaques et contient le PoC  
Fichier **Dockerfile** > permet d'exécuter l'application python dans un conteneur Docker  
Fichier **Get_started** > instructions pour utiliser rapidement le projet sans lire la documentation


