========
Émetteur
========

L'émetteur permet d'envoyer un signal, ici un signal de 433MHz. Pour le branchement,
nous avons 2 possibilités : soit brancher l'alimentation au 5V de l'arduino, soit brancher
l'alimentation au Vin de l'arduino, et alimenter l'arduino jusqu'à 12V, afin d'avoir
une meilleure émission.

Ici, j'ai fait le choix d'alimenter l'émetteur avec le Vin et d'alimenter l'arduino
avec une pile 9V. Voici le branchement :


.. figure:: Images/branchement_emetteur.png
    :align: center
    :alt: Branchement émetteur


Rejeu télécommande
------------------

Afin d'envoyer un signal avec l'émetteur, il faut envoyer les données sur
la broche DATA, et l'émetteur enverra automatiquement la donnée qui sera reçue.

Dans le contexte du rejeu du signal reçu par le récepteur, on va utiliser
les informations collectées, toujours en utilisant la librairie rcswitch.

Prenons l'exemple pour allumer la prise : le signal était 9210996 en décimal,
le protocole utilisé était le numéro 5, et le temps d'émission était de 519
microsecondes. Ce sont toutes ces informations que l'on va spécifier dans
le code.

Le signal pour éteindre la prise était 8810004 en décimal, nous allons ainsi
faire un code qui allume et qui éteint la prise toutes les 5 secondes, afin
de tester que le récepteur de la prise reçoit bien nos 2 signaux et les interprète
comme il faut.

.. code-block:: arduino

    #include <RCSwitch.h>

    RCSwitch mySwitch = RCSwitch();

    void setup() {

    Serial.begin(9600);
    
    mySwitch.enableTransmit(7); //pin de l'émetteur
    
    mySwitch.setProtocol(5);

    mySwitch.setPulseLength(519); //en microsecondes
    
    mySwitch.setRepeatTransmit(5); //nombre de répétitions
    
    }

    void loop() {

    mySwitch.send(9210996, 24); //allumer
    delay(5000);  
    mySwitch.send(8810004, 24); //éteindre
    delay(5000);

    }

Le nombre de répétitions indiqué dans le code correspond au nombre de fois où l'on
va envoyer le signal de manière consécutive. En effet, les signaux de télécommande
sont généralement envoyés plusieurs fois pour que le récepteur les reçoive correctement,
afin d'anticiper un éventuel bruit.

Après avoir téléversé et branché la carte arduino à la pile 9V, on peut observer
le bon fonctionnement de notre système. Le récepteur de la prise reçoit
correctement notre signal, le rejeu fonctionne.





