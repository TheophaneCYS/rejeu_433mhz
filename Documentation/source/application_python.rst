============================
Application Python + Arduino
============================

Cette partie va vous aider à faire fonctionner l'application python que vous pouvez trouver dans le dossier **python** du gitlab.

Prérequis
---------

Afin de faire tourner le code arduino, vous aurez besoin de la librairie **RC-Switch**. Dans l'IDE Arduino, allez dans **Outils -> Gérer les bibliothèques** et tapez *rc-switch* dans la barre de recherche puis installez là.

En ce qui concerne le code python, vous aurez besoin de la librairie **serial**, que vous pouvez installer avec pip (**sudo pip3 install pyserial**).


Branchements
------------

Trois branchements sont à réaliser pour faire fonctionner l'application :

- brancher l'émetteur à la carte arduino
- brancher le récepteur à la carte arduino
- relier la carte arduino au pc

**L'émetteur**

La broche DATA de l'émetteur (voir partie *modules_433*, mais surtout la datasheet du vôtre) se branche sur le port **12** comme indiqué au début du code arduino, mais ce n'est pas une obligation ! Vous pouvez brancher votre émetteur sur *n'importe quel port digital de votre carte arduino quelle qu'elle soit*. Excepté le port sur lequel sera le récepteur bien entendu car celui-ci ne pourra pas être changé.

.. note::

    Pensez bien à changer la valeur 12 dans le *#define EMETTEUR 12* du code arduino si vous changez de port.

Quant aux broches VCC et GND, il est conseillé de les brancher sur le Vin et le GND de l'arduino et d'alimenter l'arduino avec une pile 9V, afin qu'il émette mieux qu'en 5V. On peut aussi relier directement sur les broches de la pile avec des pinces crocodiles ou autre matériel adéquat.

**Le récepteur**

Le récepteur quant à lui s'alimente en 5V, donc on branche les broches VCC et GND aux pins 5V et GND de l'arduino. Pour la broche DATA, il faut la brancher sur le port **2** de l'arduino, et il n'est pas possible de faire autrement. Mais toutes les cartes arduino possèdent le port 2 (digital et non analog).

**Le PC**

En ce qui concerne le PC, il faut le relier à la carte arduino via le port série et bien noter le port utilisé. Le port est visible sur l'IDE arduino.


Lancement de l'application
--------------------------

Une fois les librairies installées et les branchements faits, il faut lancer le script avec python3 ou avec ./ : **./nom argument**. L'argument désigne le mode dans lequel vous souhaitez être, à savoir : écoute, rejeu ou brouillage. Il faut alors entrer comme argument *scan*, *replay* ou *jamming* pour rentrer dans un de ces modes. Pour des explications plus détaillées et une démonstration complète de l'application, vous pouvez consulter cette vidéo_.

.. _vidéo : https://www.youtube.com/watch?v=DrXHl0OH8qM

