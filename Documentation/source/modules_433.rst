==============
Modules 433MHz
==============


.. figure:: Images/433mhz.png
   :height: 415
   :width: 693
   :scale: 60
   :align: right
   :alt: Modules 433MHz

Les modules émetteur+récepteur 433MHz RF pour Arduino et ARM sont accessibles
sur de nombreux sites de vente en ligne. Ils permettent d'émettre et de recevoir
un signal sur 433MHz. Les systèmes fonctionnant à une fréquence de 433MHz sont nombreux :

- prises connectées
- alarmes (voiture, maison)
- verrouillage de portes de voiture
- démarrage de voiture sans clé
- télécommande de garage
- portails de maison
- et bien d'autres encore.


Branchements
------------

.. figure:: Images/emetteur.png
   :height: 543
   :width: 495
   :scale: 60
   :align: left
   :alt: Emetteur 433

L'émetteur se compose de 3 pins, plus un trou pour y souder une antenne.
Le pin **DATA** permet d'envoyer les données à transmettre.




Le récepteur quant à lui se compose de 4 pins, plus d'un trou pour y souder une antenne.
Il comprend 2 pins **DATA**, permettant d'envoyer les données reçues à la carte.
Les données sont envoyées sur les 2 pins, on peut ainsi choisir arbitrairement
laquelle sera reliée à la carte.

.. figure:: Images/recepteur.png
   :height: 358
   :width: 508
   :scale: 70
   :align: right
   :alt: Recepteur 433


Antenne
-------

On peut fixer une antenne sur l'émetteur et le récepteur afin d'améliorer la portée
d'émission/réception.
La taille de l'antenne doit être de 17.3cm pour une meilleure qualité. En effet,
la taille optimale d'une antenne est 1/4 de la longueur d'onde.
La longueur d'onde est égale à la célérité de l'onde divisé par sa fréquence,
soit *3.10⁸ / 433.10⁶ = 0.693m*. La longueur d'onde est donc de 69.3cm,
et *69.3/4 = 17.3*.

.. note::

   Si vous ne disposez pas d'antenne, un fil de fer peut en faire l'usage.


Alimentation
------------

Le récepteur fonctionne avec une alimentation de 5V. Quant à l'émetteur, il fonctionne
à une alimentation allant de 3.3V à 9V. Plus la tension est élevée, plus le signal
sera émis loin.




