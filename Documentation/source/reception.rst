=========
Récepteur
=========

Le récepteur est le premier des deux modules que nous allons utiliser.
Dans le cas d'un rejeu, nous voulons tout d'abord recevoir le signal, puis le rejouer.
On va ainsi voir comment correctement recevoir notre signal.

Détection du signal
-------------------

Tout d'abord, il faut s'assurer que l'émetteur que nous voulons rejouer (télécommande,
clé de voiture, ...) fonctionne bien en 433MHz. Pour cela, on peut écrire
un petit code arduino qui affiche sur le lien série ce que le récepteur reçoit.

Le code serait le suivant :

.. code-block:: arduino

    #define DATA 7      //pin du récepteur

    void setup() {
        pinMode(DATA, INPUT);
        Serial.begin(9600);     //initialisation du lien série
    }

    void loop() {
        Serial.println(digitalRead(DATA));
    }

Et le branchement :

.. figure:: Images/branchement_recepteur.png
    :height: 712
    :width: 673
    :scale: 80
    :align: center
    :alt: Branchement récepteur



On peut ensuite téléverser le programme arduino dans la carte et ouvrir le
traceur série (Outils -> Traceur série).


.. figure:: Images/bruit.png
    :align: center
    :alt: Traceur série bruit

On observe un bruit constant sur le lien série. Afin de savoir si notre
télécommande ou autre émetteur fonctionne en 433MHz, on va émettre et observer
ce qu'il se passe sur le lien série.

Voici ce que l'on obtient en pressant un des boutons d'une télécommande pour prises
connectées :

.. figure:: Images/signal_telecommande.png
    :align: center
    :alt: Traceur série télécommande


On constate ainsi que la télécommande fonctionne bien en 433MHz, vu que l'on a
capté un signal. La difficulté va maintenant être d'extraire ce signal afin
de rejouer exactement le même avec notre émetteur.

Une technique assez approximative serait de regarder les bits reçus sur le
moniteur série. L'inconvénient est que l'on a beaucoup de bruits, et que l'on
ne connait pas la période du signal (on ne sait pas combien de temps sépare
chaque bit, ni le temps total d'émission).

Une autre méthode est d'utiliser des librairies arduino qui calculent le temps
d'émission et permettent de commander facilement les modules d'émission et de réception.
On constate que de nombreuses librairies existent dans le domaine.

Télécommande pour prises connectées
-----------------------------------

Si vous souhaitez rejouer un signal émis par une télécommande, la librairie
rc_switch peut être utilisée. Cette librairie reconnaît le protocole que la télécommande
utilise et calcule la durée du signal.

Nous allons voir un exemple ici avec une télécommande qui commande des prises
électriques. Le système est le même pour les anciennes télécommandes de garage,
avant que celui-ci ne soit remplacé par un système sécurisé.

Dans cette partie nous étudierons comment recevoir le signal de la télécommande, et
dans la partie émission nous verrons comment l'émettre.

.. figure:: Images/telecommande.png
    :align: center
    :alt: Télécommande et prise utilisées

Branchement
~~~~~~~~~~~

Le branchement est le même que précédemment, excepté que nous branchons
le récepteur sur la pin numéro 2. Une antenne est conseillée afin de
mieux recevoir le signal.


Code
~~~~

.. code-block:: arduino

    //Écoute du récepteur

    #include <RCSwitch.h>

    RCSwitch mySwitch = RCSwitch();

    void setup() {
        Serial.begin(9600);
        mySwitch.enableReceive(0);  // Receiver on interrupt 0 => that is pin #2
    }

    void loop() {
        if (mySwitch.available()) {
            
            Serial.print("Reçu : ");
            Serial.print( mySwitch.getReceivedValue() );
            Serial.print(" / ");
            Serial.print( mySwitch.getReceivedBitlength() );
            Serial.print("bit ");
            Serial.print("Protocole: ");
            Serial.println( mySwitch.getReceivedProtocol() );
            Serial.print("temps : ");
            Serial.print( mySwitch.getReceivedDelay() );
            Serial.println("microsecondes");
            mySwitch.resetAvailable();
        }
    }

Comme expliqué précédemment, il est intéressant d'utiliser une bibliothèque pour lire
ce qui est reçu par le récepteur et avoir de meilleures informations. Dans le code,
on créé un objet de type RCSwitch, issu de la librairie rcswitch, et on l'initialise
en indiquant qu'il doit "écouter" sur la pin numéro 2. Lorsqu'un signal sera détecté
sur cette pin, on affichera sur le moniteur série certaines informations utiles : le signal
reçu sous forme décimal, sa longueur, le protocole utilisé et le temps d'émission.

Résultat
~~~~~~~~

Le code a été téléversé sur une arduino Uno et une télécommande a été utilisée pour le tester.
Dans la capture ci-dessous, le bouton OFF de la prise B de la télécommande a été pressé,
puis le bouton ON.


.. figure:: Images/B_off_on.png
    :align: center
    :alt: réception télécommande


En affichant le signal converti en décimal, on peut voir que la télécommande envoie
8810004 pour éteindre la prise, et 9210996 pour l'allumer. La librairie utilisée
nous indique aussi le protocole qu'utilise la télécommande. Cette librairie détecte 
5 protocoles différents. Ces protocoles décrivent comment est envoyé le signal et 
comment nous devons le lire, par exemple : LSB first ou MSB first. Malheureusement,
on ne trouve pas sur le github de la librairie d'explications sur quel numéro
correspond à quel protocole. Puis nous avons le temps d'émission,
et en utilisant toutes ces informations nous pourrons rejouer correctement le signal
dans le code de l'émetteur.
