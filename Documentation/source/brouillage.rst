==========
Brouillage
==========


Après avoir correctement rejoué un signal de télécommande, nous allons
mettre en place du brouillage afin que le récepteur des prises connectées
ne reçoive pas les signaux de la télécommande.

Pour cela, la fonction **tone(broche, frequence)** d'arduino peut être utilisée. Cette fonction
permet de générer un signal carré de type *duty cycle* (50% état haut, 50% état bas
sur une période). On va alors envoyer ce signal sur l'émetteur, en mettant
plusieurs fréquences différentes pour avoir un meilleur brouillage.

Le code du brouillage est alors tout simple :

.. code-block:: arduino

    void setup() {
    }

    void loop() {

    tone(8, 6700); // generate square wave
    tone(8, 10000);
    tone(8, 1000);
    }


Les fréquences utilisées viennent d'un blog (https://www.blogtech.pl/index.php/en/2015/10/21/jammer-433mhz-arduino-mini-pro-2/)
et d'une vidéo Youtube (https://www.youtube.com/watch?v=j7vufor83as) où
l'auteur a indiqué qu'il avait choisi les valeurs qui fonctionnaient le mieux
pour 433MHz. Mais d'autres valeurs peuvent être utilisées pour le brouillage.
Je vous conseille néanmoins ces valeurs qui fonctionnent pour ma télécommande
de prises connectées, celle-ci devient inutilisable lorsque l'émetteur utilise
ce brouillage.


On peut également visualiser avec le récepteur le signal de brouillage, comme nous
l'avons vu dans la partie réception.

Ce que reçoit le récepteur **sans** le brouillage :

.. figure:: Images/bruit.png
    :align: center
    :alt: Traceur série bruit


Ce qu'il reçoit **avec** le brouillage à proximité :

.. figure:: Images/bruit_tone2.png
    :align: center
    :alt: Traceur série bruit tone



