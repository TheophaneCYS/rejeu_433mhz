# README 
 
## Documentation Rejeu 433MHz


Cette documentation explique comment utiliser des modules émetteur/récepteur 433MHz afin de réaliser des attaques de rejeu sur des systèmes fonctionnant à cette fréquence. Parmi ces systèmes, on peut trouver entre autres :
* les télécommandes de prises connectées
* les télécommandes de garage
* les systèmes de verrouillage de portes de voiture



### Compiler la documentation

Pour lire la documentation, téléchargez là et aller à l'aide d'un terminal dans le dossier principal contenant le Makefile.

**Compiler en html** :   

```bash
$ make html
$ firefox build/html/index.html
```
**Compiler en pdf LaTeX** :

```bash
$ make latexpdf
```
Le pdf se situe dans build/latex.
